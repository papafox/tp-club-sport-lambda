<?php

/**
 * Classe de la connexion à la base de données
 */

 include_once('Seance.php');
 include_once('User.php');

 class Database{

    // Constantes de connexion
    const DB_HOST = "mariadb";
    const DB_PORT = "3306";
    const DB_NAME = "clublambda";
    const DB_USER = "adminClub";
    const DB_PASSWORD = "@dminClub";

    // Attribut de la classe 
    private $connexion;

    //Constructeur pour initier la connexion
    public function __construct(){
        try {
            $this->connexion = new
            PDO("mysql:host=".self::DB_HOST.";port=".self::DB_PORT.";dbname=".self::DB_NAME.";charset=UTF8",
                                    self::DB_USER,
                                    self::DB_PASSWORD);
        } catch (PDOExeption $e) {
            echo 'Connexion échouée : ' . $e->getMessage();
        }
    }
    /**
     * Fonction pour créer une nouvelle séance en base de données
     * 
     * @param{Seance} seance : la séance à sauvegarder
     * 
     * @return{integer, boolean} l'id si la séance a été créée ou false sinon
     */
    public function createSeance(Seance $seance){
        // Je prépare ma requête SQL
        $pdoStatement = $this->connexion->prepare(
            "INSERT INTO seances(titre, description, heureDebut, date, duree, nbParticipantsMax, couleur)
             VALUES (:titre, :description, :heureDebut, :date, :duree, :nbParticipantsMax, :couleur)"
        );
        // J'execute ma requête en passant les valeurs de l'objet Seance en valeur
        $pdoStatement->execute([
            "titre"             => $seance->getTitre(),
            "description"       => $seance->getDescription(),
            "heureDebut"        => $seance->getHeureDebut(),
            "date"              => $seance->getDate(),
            "duree"             => $seance->getDuree(),
            "nbParticipantsMax" => $seance->getNbParticipantsMax(),
            "couleur"           => $seance->getCouleur()
        ]); 
        // Je récupère l'id créé si l'exclusion s'est bien passée (code 0000 de MySQL)
        if($pdoStatement->errerCode() == 0){
            $id = $this->connexion->lastInsertId();
            return $id;
        } else{
            return false;
        }
    }
    
}

?>

