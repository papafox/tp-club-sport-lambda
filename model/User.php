<?php

class User {
    //Attributs de la classe
    private $id;
    private $nom;
    private $email;
    private $password;
    private $isAdmin;
    private $isActif;
    private $token;

    //Contructeur par défaut
    public function __construct(){}

    //Constructeur acceptant toutes es valeurs de l'objet
    public static function createUser($nom, $email, $password, $isAdmin, $isActif, $token){
        $user = new self();
        $user->setNom($nom);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setIsAdmin($isAdmin);
        $user->setIsActif($isActif);
        $user->setToken($token);
    }

    // Getters
    public function getId(){ return $this->id; }
    public function getNom(){ return $this->nom; }
    public function getEmail(){ return $this->email; }
    public function getPassword(){ return $this->password; }
    public function getIsAdmin(){ return $this->isAdmin; }
    public function getIsActif(){ return $this->isActif; }
    public function getToken(){ return $this->token; }

    // Setters
    public function setId($id){ $this->id = $id; }
    public function setNom($nom){ $this->nom = $nom; }
    public function setEmail($email){ $this->emal = $email; }
    public function setPassword($password){ $this->password = $password; }
    public function setIsAdmin($isAdmin){ $this->isAdmin = $isAdmin; }
    public function setIsActif($isActif){ $this->isActif = $isActif; }
    public function setToken($token){ $this->token = $token; }
}