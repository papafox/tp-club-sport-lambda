<?php
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Modifier Pilates</h1>
    <div class="card-body">
        <form class="text-left text-md-right" action="" method="POST">
            <div class="form-group row"> 
                <label for="nom" class="col-sm-12 col-md-4 col-form-label">Nom</label>
                <div class="col-sm-12 col-md-8">
                    <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom du 
                    cours" value="Pilates" required>
                </div>
            </div>
            <div class="form-group row"> 
                <label for="couleur" class="col-sm-12 col-md-4 col-form-label">Couleur de fond</label>
                <div class="col-sm-12 col-md-8">
                    <input type="color" class="form-control" id="couleur" name="couleur" 
                    placeholder="Couleur" value="#03bafc" required>
                </div>
            </div>
            <div class="form-group row"> 
                <label for="date" class="col-sm-12 col-md-4 col-form-label">Date</label>
                <div class="col-sm-12 col-md-8">
                    <input type="date" class="form-control" id="date" name="date" value="2019-01-10" 
                    required>
                </div>
            </div>
            <div class="form-group row"> 
                <label for="heureDebut" class="col-sm-12 col-md-4 col-form-label">Heure de début</label>
                <div class="col-sm-12 col-md-8">
                    <input type="time" class="form-control" id="heureDebut" name="heureDebut" 
                    value="09:00" required>
                </div>
            </div>
            <div class="form-group row"> 
                <label for="duree" class="col-sm-12 col-md-4 col-form-label">Durée</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="duree" name="duree" 
                    value="50" required><span>minutes</span>
                </div>
            </div>
            <div class="form-group row"> 
                <label for="description" class="col-sm-12 col-md-4 col-form-label">Description</label>
                <div class="col-sm-12 col-md-8">
                    <textarea class="form-control" id="description" name="description">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque 
                        laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi 
                        architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas 
                        sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione 
                        voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, 
                        consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore 
                        magnam aliquam quaerat voluptatem.
                    </textarea>
                </div>
            </div>
            <div class="form-group row"> 
                <label for="nbParticipants" class="col-sm-12 col-md-4 col-form-label">Nombre de 
                participants max</label>
                <div class="col-sm-12 col-md-8">
                    <input type="number" class="form-control" id="ndParticipants" name="nbParticipants" 
                    value="20" required><span>participants</span>
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-dark" type="submit">Modifier</button>
            </div>
        </form>
    </div>
</div>

<?php
include('modules/partie3.php');
?>



            

