<footer>
    <div class="container-fluid bg-info text-white pt-2">
        <div class="row text-center text-xs-center text-sm-left text-md-left">
            <div class="col-xs-12 col-sm-4 col-md-4">
                <h5>Liens</h5>
                <ul class="list-unstyled">
                    <li><a href="login.php">Login</a></li>
                    <li><a href="inscription.php">Inscription</a></li>
                    <li><a href="planning.php">Planning</a></li>
                    <li><a href="#">Créer cours</a></li>
                    <li><a href="#">Profil</a></li>
                    <li><a href="#">Déconnexion</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center badge badge-dark">
            <p class="h6">&copy Tous droits réservés. <a class="ml-2" href="http://www.studio636.ch" 
            target="blank">studio636</a></p>
        </div>
        </hr>
    </div>
</footer>