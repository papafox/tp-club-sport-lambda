<?php
    include('modules/partie1.php');
?>

<div class="container card text-center mt-4">
    <h1 class="card-header">Pilates</h1>
    <div class="card-body text-left" style="background: #03bafc;">
        <div class="row">
            <div class="offset-3 col-9">
                <p>Date : 10/01/2019</p>
            </div>
            <div class="offset-3 col-9">
                <p>Heure de début : 09h00</p>
            </div>
            <div class="offset-3 col-9">
                <p>Durée : 50 minutes</p>
            </div>
            <div class="offset-3 col-9">
                <p>Description :</p>
            </div>
            <div class="offset-3 col-9">
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, 
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p> 
            </div>
            <div class="offset-3 col-9">
                <p>Nombre de participants max : 20</p>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                    <a class="btn btn-danger" href="#">Se désinscrire</a>
                    <a class="btn btn-primary" href="#">S'inscrire</a>
                    <a class="btn btn-danger" href="#">Complet</a>
                </div>
            </div>
            <div class="col-12">
                <div class="d-flex justify-content-around m-2">
                    <a class="btn btn-warning" href="#">Dupliquer</a>
                    <a class="btn btn-primary" href="#">Modifier</a>
                    <a class="btn btn-danger" href="#">Supprimer</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include('modules/partie3.php');
?>
